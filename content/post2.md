Title: Công thức nấu bò kho tại nhà
Date: 2020-09-28
Category: Trang chủ
Tags: python, misc, cooking, beef
Slug: second
Authors: Biingo
Summary: chuyenbepnuc

## Công thức nấu bò kho ngon, bổ, rẻ 
### Nguyên liệu làm bò kho
- Thịt nạm bò 1kg (phần thịt có chứa gân bò)

- Cà rốt: 500g

- Bánh mì

- Dầu điều

- 2 Gói bột gia vị bò kho, bột năng

- Sả, gừng, hành khô và tỏi

- Ớt tươi, hoa hồi

- Gia vị: Muối, dầu ăn, đường, hạt tiêu, bột ngọt

- Rau húng quế, ngò gai

<img src='https://cdn.eva.vn/upload/4-2018/images/2018-12-20/cach-nau-bo-kho-ngon-don-gian-de-lam-ai-an-cung-khen-tam-tac-image001-1545294939-12-width419height378.jpg'>

### Các bước thực hiện
#### Bước 1: Sơ chế nguyên liệu
- Khử mùi hôi và làm sạch thịt bò: Lấy mẫu gừng đập dập pha với một chút rượu thêm chút muối sau đó cho thịt bò vào bóp đều rồi rửa sạch. Tiếp đó cho thịt vào chần qua nước nóng trong vài phút rồi vớt ra để ráo nước, thái miếng vuông vừa ăn.

- Rau củ quả gọt vỏ, nhặt sạch. Sả cắt khúc đập dập, cà rốt cắt khúc hoặc tỉa hoa cho đẹp. Hành khô, gừng, tỏi đập dập băm nhỏ

- Hoa hồi rang thơm để cả hoa, không nên giã nhỏ khi ăn thịt bò sẽ bị đắng

Chú ý: Bí quyết nấu món bò kho ngon chính là chọn phần thịt nạm bò vì có chứa cả gân. Kho xong ăn thịt mềm, gân bò ăn giòn béo hơn mà không bị nát so với thịt toàn nạc ăn dễ ngán.
#### Bước 2: Ướp thịt bò
Thịt bò sau khi thái miếng cho vào ướp với 2 thìa cà phê muối, 2 thìa đường, 2 thìa bột gia vị bò kho, hạt tiêu, thêm chút dầu điều sau đó cho ½ tỏi, gừng và hành băm vào rồi trộn đều để khoảng 25 phút cho thịt ngấm đều gia vị. (Thêm ớt, sa tế nếu bạn thích ăn cay).

<img src='https://cdn.eva.vn/upload/4-2018/images/2018-12-20/cach-nau-bo-kho-ngon-don-gian-de-lam-ai-an-cung-khen-tam-tac-image003-1545295086-145-width800height600.jpg' >

#### Bước 3: Nấu bò kho
- Bắc nồi lên bếp, cho dầu ăn vào đun nóng rồi cho sả, ½ tỏi, gừng và hành băm còn lại vào phi thơm. Không nên dùng dầu điều để phi thơm gia vị vì khi gặp nhiệt cao, dầu điều sẽ cháy khét gây biến chất.

- Tiếp đó trút thịt bò đã ướp gia vị vào xào, đảo đều đến khi thịt săn lại thì cho thêm chút dầu điều để lên màu cho món ăn hấp dẫn. Sau đó chế nước lọc vào nồi cho xâm xấp mặt thịt tiếp rồi cho hoa hồi vào đun sôi nhỏ lửa khoảng 20 phút rồi vớt hoa hồi ra. Có thể dùng nồi áp suất để kho cho thịt bò nhanh mềm.

- Vớt hoa hồi xong, lấy 4 thìa bột năng hòa vào bát con nước lọc khuấy đều sau đó chế từ từ vào nồi đun sôi đến khi sền sệt. Nêm nếm gia vị sao cho vừa ăn rồi cho cà rốt vào đun nhỏ lửa trong vòng 10 phút là cà rốt chín mềm.

Như vậy, cách nấu bò kho đã xong, thịt mềm ăn giòn ngọt mà không nát, nước kho sền sệt đậm màu vô cùng hấp dẫn chắc chắn ai ăn cũng tấm tắc khen ngon.

<img src='https://cdn.eva.vn/upload/4-2018/images/2018-12-20/cach-nau-bo-kho-ngon-don-gian-de-lam-ai-an-cung-khen-tam-tac-image005-1545295086-557-width700height600.jpg' >

Món bò kho ngon nhất này ăn kèm với bánh mì, xé miếng nhỏ chấm nước sốt kho sền sệt ăn kèm với thịt bò, rau húng quế và chút ngò gai sẽ có vị thơm ngọt bùi béo ngậy mà không ngấy hoặc ăn với cơm nóng sẽ rất tuyệt. Có thể bảo quản thịt bò kho trong ngăn mát tủ lạnh, sử dụng trong 2 đến 3 ngày.

## Bon Appetit!